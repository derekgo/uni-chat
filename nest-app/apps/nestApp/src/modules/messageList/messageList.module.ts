import { Module } from '@nestjs/common';
import { MessageListService } from './messageList.service';
import { MessageListController } from './messageList.controller';
import {TypeOrmModule} from "@nestjs/typeorm";
import {MessageListEntity} from "./entities/messageList.entity";

@Module({
  imports:[TypeOrmModule.forFeature([MessageListEntity])],
  controllers: [MessageListController],
  providers: [MessageListService]
})
export class MessageListModule {}
