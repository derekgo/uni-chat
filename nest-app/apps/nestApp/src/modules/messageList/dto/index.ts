/*- coding = utf-8 -*-
@Time : 2023/2/1 10:19
@Author : 沉默小管
@File : index.ts
@web  : golangblog.blog.csdn.net
@Software: WebStorm
*/

export {MessageListAddDto} from "./messageListAdd.dto"
export {MessageListDto} from "./messageList.dto"
export {MessageListUpdateDto} from "./messageListUpdate.dto"
export {MessageListDelDto} from "./messageListDel.dto"
