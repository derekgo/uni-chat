import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import { BaseEntity } from "@/apps/common/commonModules/entities/base.entity";

@Entity('g_artList')
export class UserEntity extends BaseEntity{
    @PrimaryGeneratedColumn({
        comment:"用户列表",
    })//主键
    id:number;

    @Column({
        type:"varchar",
        length:100,
        nullable:true,
        comment:"用户名称",
    })
    nickName: string;

    @Column({
        type:"varchar",
        length:100,
        nullable:true,
        comment:"个性签名(限100字)",
    })
    signature: string;

    @Column({
        type:"varchar",
        length:100,
        nullable:true,
        comment:"用户头像",
    })
    headImg: string;

    @Column({
        type:"varchar",
        length:100,
        nullable:true,
        comment:"账号",
    })
    username: string;

    @Column({
        type:"varchar",
        length:100,
        nullable:true,
        comment:"原始密码",
    })
    originalPwd: string;

    @Column({
        type:"varchar",
        length:100,
        nullable:true,
        comment:"密码",
    })
    password: string;

    @Column({
        type:"varchar",
        length:150,
        nullable:true,
        comment:"邮箱",
    })
    email: string;

    @Column({
        type:"varchar",
        length:50,
        nullable:true,
        comment:"手机号",
    })
    phone: string;

    @Column({
        type:"int",
        nullable:true,
        comment:"年龄",
    })
    age: number;

    @Column({
        type:"tinyint",
        nullable:true,
        comment:"性别 1.男，2.女",
    })
    sex: string;

    @Column({
        type:"varchar",
        length:50,
        nullable:true,
        comment:"角色过期时间",
    })
    roleExpireTime: string;

    @Column({
        type:"int",
        nullable:true,
        comment:"角色id//默认普通用户。普通用户,铂晶用户,钻石用户",
    })
    roleId: number;

    @Column({
        type:"varchar",
        nullable:true,
        comment:"菜菜币//通过菜菜币判断当前用户是否为新老用户，如果低于0讲停止聊天服务(需要通知管理员恢复)。如果做了违禁行为将扣除菜菜币。每天登陆+1菜菜币",
    })
    ccbNum: string;

    @Column({
        type:"varchar",
        nullable:true,
        comment:"通过jwt生成token，拿token里的过期时间判断是否在线，jwt生成添加电脑的唯一标识(mac)",
    })
    token: string;

    @Column({
        type:"int",
        nullable:true,
        comment:"排序",
    })
    sort: number;

}
