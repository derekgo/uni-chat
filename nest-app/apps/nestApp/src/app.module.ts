import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ClientsModule, Transport } from "@nestjs/microservices";
import { CommentModule } from "@/modules/comment/comment.module";
import { FriendListModule } from "@/modules/friendList/friendList.module";
import { MessageModule } from "@/modules/messageList/messageList.module";
import { GroupListModule } from "@/modules/groupList/groupList.module";
import { RoleListModule } from "@/modules/roleList/roleList.module";
import { UserListModule } from "@/modules/userList/userList.module";
import { WorldGroupChatModule } from "@/modules/worldGroupChat/worldGroupChat.module";
import { join } from "path";
import { ServeStaticModule } from "@nestjs/serve-static";
import { TypeOrmModule } from "@nestjs/typeorm";
import { mysqlConfig } from "@/apps/common/utils/config";
import { ScheduleModule } from "@nestjs/schedule";

let mysql =  TypeOrmModule.forRoot(mysqlConfig);

@Module({
  imports: [ClientsModule.register([
    {
      name: 'CALC_SERVICE',
      transport: Transport.TCP,
      options: {
        port: 8888,
      },
    },
  ]),ServeStaticModule.forRoot({
    rootPath: join(__dirname, '..', 'public/uploads'),
    serveRoot: '/static',
  }),mysql,ScheduleModule.forRoot(),WorldGroupChatModule,RoleListModule,GroupListModule, CommentModule, FriendListModule,MessageModule,UserListModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
