/*- coding = utf-8 -*-
@Time : 2023/4/17 15:09
@Author : 沉默小管
@File : index.ts
@web  : golangblog.blog.csdn.net
@Software: WebStorm
*/
// 公共枚举文件

//登陆状态枚举
enum loginStatusEnum{
    success=1,
    fail=2,
}

//操作类型枚举
enum operationTypeEnum{
    add = 1,//添加
    delete = 2,//删除
    search = 3,//查询
    edit = 4,//修改
    other = 5,//其他
    auth = 6,//授权
    export = 7,//导出
    import = 8,//导入
    forcedRetreat = 9,//强退
    generating = 10,//生成代码
    clearData = 11,//清空数据
}
//性别枚举
enum sexEnum{
    man = 1,//男
    female = 2,//女
    unknown = 3,//未知
}
//状态枚举
enum statusEnum{
    start = 1,//启用
    stop = 2,//停用
}
//通告状态枚举
enum notifyStatusEnum{
    read = 1,//启用
    unread = 2,//停用
}
//状态类型枚举
enum menuTypeEnum{
    directory = 1,//目录
    menu = 2,//菜单
    button = 3,//按钮
}
//其他通用状态枚举
enum otherStatusEnum{
    yes = 1,//是
    no = 2,//不
}

//账号多地登陆状态枚举
enum multipleLoginAccountsStatusEnum{
    open = 1,//开启
    close = 2,//关闭
}
//账号登录状态
enum userLoginStatusEnum{
    Online = 1,//开启
    Offline = 2,//关闭
}

export {userLoginStatusEnum,multipleLoginAccountsStatusEnum,notifyStatusEnum,otherStatusEnum,loginStatusEnum,menuTypeEnum,statusEnum,operationTypeEnum,sexEnum}