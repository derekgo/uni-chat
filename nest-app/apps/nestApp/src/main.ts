import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { mainService } from "../../common/utils/config";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(mainService.port);
}
bootstrap().then(r =>  console.log(`http://localhost:${mainService.port}`));
