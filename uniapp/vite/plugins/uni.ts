import { Plugin } from 'vite'
import uni from '@dcloudio/vite-plugin-uni'

//vue框架
export default (plugins: Plugin[], isBuild: boolean) => {
    plugins.push(uni())
}
