//*- coding = utf-8 -*-
//@Time : 2022-10-19 0:20
//@Author : 管茂良
//@File : index.js
//@web  : www.php-china.com
//@Software: WebStorm
import {request} from "@/network/request"

//登录
export function requestLogin(data:any){
    return request({
        url:"/user/login",
        methods:"POST",
        data,
    })
}
//退出
export function requestLogout(data:any){
    return request({
        url:"/user/logout",
        methods:"POST",
        data,
    })
}
//获取信息
export function requestGetInfo(data:any){
    return request({
        url:"/user/userInfo",
        methods:"POST",
        data,
    })
}
//登录没有验证码
export function requestLoginNoCode(data:any){
    return request({
        url:"/captchaImage",
        methods:"GET",
        data,
    })
}