/*- coding = utf-8 -*-
@Time : 2022/9/21 11:45
@Author : 管茂良
@File : request.tsx
@web  : www.php-china.com
@Software: WebStorm
*/
import baseConfig from "@/utils/config"
import {getToken} from "@/utils/storage";

type methodsType = 'GET' | 'POST' | 'PUT'

const request = (config:{methods:methodsType,url:string,data?:any,headers?:object})=>{
    let {methods,data,url} = config
    let TOKEN = getToken();
    if(TOKEN){
        config.headers={
            'X-CSRF-TOKEN' : `${TOKEN}`,
            'Authorization' : `token`,
            'content-type' : "application/x-www-form-urlencoded"
        }
    }
    config.headers = {
        'content-type' : "application/x-www-form-urlencoded"
    }
    return new Promise((resolve,reject)=>{
        uni.request({
            method:methods,
            url: baseConfig.BaseURL + url,
            data,
            header:config.headers,
            dataType: 'json',
            sslVerify: false,
            success: (res:any)=>{
                resolve(res)
            },
            fail: (res:any)=>{
                reject(res)
            },
        })
    })
}

export {request}