//*- coding = utf-8 -*-
//@Time : 2022-10-18 11:17
//@Author : 管茂良
//@File : config.js
//@web  : www.php-china.com
//@Software: WebStorm

export default {
    //基础域名
    BaseURL:"https://zy.nbnkyy.com/",
    BaseURLStorage: "https://zy.nbnkyy.com/",
    //项目版本
    ItemVersion:"v1",
    //项目名称
    ItemName:"沉默小管",
    //项目大小
    SysSize:"small",
    //主题
    Theme:"#FFFFFF",
    //项目语言
    Language:"zh",
}
