/*- coding = utf-8 -*-
@Time : 2023/7/5 17:14
@Author : 管茂良
@File : index.ts
@web  : www.php-china.com
@Software: WebStorm
*/

export type typeType = "default" | "primary" | "success" | "warning" | "info"
