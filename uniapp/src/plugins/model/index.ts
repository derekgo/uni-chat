/*- coding = utf-8 -*-
@Time : 2022/9/25 17:27
@Author : CSDN 沉默小管
@File : useClass.ts
@web  : golangblog.blog.csdn.net
@Software: WebStorm
*/

import loading from "@/plugins/model/loading/index"
import modal from "@/plugins/model/modal/index"
import toast from "@/plugins/model/toast/index"
import notify from "@/plugins/model/notify/index"

export {loading,modal,toast,notify}

