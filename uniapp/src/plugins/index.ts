//*- coding = utf-8 -*-
//@Time : 2022-10-18 11:30
//@Author : 管茂良
//@File : index.js
//@web  : www.php-china.com
//@Software: WebStorm
import {App} from "vue"
import uView from "./uView"
import i18n from "./i18n"
import axios from "axios";
import SvgIcon from "@/components/svgIcon/index";
import {loading,modal,toast,notify} from "./model"

const models = [uView,i18n]

export default (app:App)=>{
    app.config.globalProperties.$axios = axios
    app.config.globalProperties.$loading = loading()
    app.config.globalProperties.$modal = modal()
    app.config.globalProperties.$toast = toast()
    app.config.globalProperties.$notify = notify()

    app.component("SvgIcon",SvgIcon)

    models.map((val)=>{
        return val(app)
    })
}
