/*- coding = utf-8 -*-
@Time : 2022/9/21 11:44
@Author : 管茂良
@File : index.tsx
@web  : www.php-china.com
@Software: WebStorm
*/
import {createPinia,defineStore,storeToRefs} from "pinia"
import appStore from "./modules/app"
import userStore from "./modules/User"


export {
    createPinia,
    storeToRefs,
    userStore,
    appStore
}


