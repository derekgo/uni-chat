/*- coding = utf-8 -*-
@Time : 2022/9/21 14:28
@Author : 管茂良
@File : storeName.tsx
@web  : www.php-china.com
@Software: WebStorm
*/
const storeName = {
    app:"appStore",
    user:"userStore"
}
export default storeName