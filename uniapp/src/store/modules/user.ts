/*- coding = utf-8 -*-
@Time : 2022/9/21 14:27
@Author : 管茂良
@File : Form.tsx
@web  : www.php-china.com
@Software: WebStorm
*/
//用户状态

import {defineStore} from "pinia"
import storeName from "@/store/storeName"
import {getToken, removeToken, removeUserId, removeUsername, setToken} from "@/utils/storage";
import {requestGetInfo, requestLogin, requestLogout} from "@/network/me/login";

interface userInterface{
    token:string
    userInfo:any
    avatar: string
    roles: any[],
    permissions: any[],
    sysConfig:any[],
}

const useUserStore = defineStore(storeName.user,{
    state:():userInterface=>({
        token: getToken(),
        userInfo: {},
        avatar: '',
        roles: [],
        permissions: [],
        sysConfig: [],
    }),
    getters:{

    },
    actions:{
// 登录
        Login(userInfo: { username: string; password: any; code: any; uuid: any; }) {
            const username = userInfo.username.trim()
            const password = userInfo.password
            const code = userInfo.code
            const uuid = userInfo.uuid
            return new Promise<void>((resolve, reject) => {
                requestLogin({username, password, code, uuid}).then((res: { data: { token: any; }; }) => {
                    setToken(res.data.token)
                    this.token = res.data.token
                    resolve()
                }).catch((error: any) => {
                    reject(error)
                })
            })
        },

        // 获取用户信息
        GetInfo() {
            return new Promise<any>((resolve, reject) => {
                requestGetInfo({token:this.token}).then((res: any) => {
                    const userInfo = res.data.userInfo
                    const role = res.data.roles
                    const permissions = res.data.permissions
                    const sysConfig = res.data.sysConfig
                    // const avatar = userList.avatar == "" ? new URL(`../../../assets/v1/img/profile.jpg`, import.meta.url).href : process.env.VUE_APP_BASE_API + userList.avatar;
                    if (role && role.length > 0) { // 验证返回的roles是否是一个非空数组
                        this.roles = role
                        this.permissions = permissions
                    } else {
                        this.roles = ['ROLE_DEFAULT']
                    }
                    this.sysConfig = sysConfig
                    this.userInfo = userInfo
                    // this.avatar = avatar
                    //判断是否开启系统，没有开启系统，除了超级管理员，其他都不能登录
                    if(this.sysConfig["sysStatus"]==1 || this.roles.includes("admin")){
                        resolve(res)
                    }else{
                        reject("系统已关闭，请联系管理员")
                    }
                }).catch((error: any) => {
                    reject(error)
                })
            })
        },

        // 退出系统
        LogOut() {
            return new Promise<any>((resolve, reject) => {
                let uid = this.userInfo?.id
                requestLogout({uid}).then((res: any) => {
                    this.token = ""
                    this.roles = []
                    this.permissions = []
                    removeUsername()
                    removeUserId()
                    removeToken()
                    resolve(true)
                }).catch(err=>{
                    reject(err)
                })

            })
        }
    }
})

export default useUserStore
